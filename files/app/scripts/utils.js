/*  
    ¡No modificar!
    Eventos y funciones útiles que no requieren un módulo individual.
*/
const presentation = require('presentation');
const isMobile = require('is-mobile');
const _ = require('lodash');
const screenfull = require('screenfull')

module.exports = function (args) {
    if (isMobile()) {
        $('html').addClass('is-mobile');
    }

    $('body').on('click', '.click-me', function () {
        $(this).addClass('clicked')
    })

    $('body').on('mouseenter', '.hover-me', function () {
        $(this).addClass('hovered')
    })

    $('body').on('click', '[data-switch-to-slide]', function () {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        var target = $(this).attr('data-switch-to-slide');
        var directions = ['prev', 'home', 'next'];
        var slide = null;
        if(_.includes(directions, target)){
            slide = target;
        }
        else{
            slide = $('.slide[data-slide="' + target + '"]').eq(0)
        }
        presentation.switchToSlide({
           slide: slide 
        })
    });

    $('.secondary-info .glossary').on('click', function () {
        args.ovaConcepts.showEarnedConceptsModal()
    });

    _.forEach(args.ovaConfig.slidesConfig.disabledSlides, function (v, k) {
        $('.slide[data-slide="' + v + '"]').data('presentation').enabled = false;
    });

    $('.ova-content-header .fullscreen').on('click', function(){
        if (screenfull.enabled){
            screenfull.toggle();
        }
    })

    $('body').on('mouseenter', '*', function(){
        if($(this).hasClass('click-me') && $(this).hasClass('animated')){
            $(this).removeClass('animated')
        }
    })

    $('.slide .navigation-buttons').children(':not([data-switch-to-slide])').each(function(){
        $(this).on('click', function(){
            var validClasses = ['next', 'prev', 'home'];
            var classes = $(this).attr('class').split(' ');
            var selectedClass = null;
            _.forEach(classes, function(v, k){
                if(_.includes(validClasses, v)){
                    selectedClass = v;
                }
            })

            if(_.isString(selectedClass)){
                presentation.switchToSlide({
                    slide: selectedClass
                })
            }
        })
    })

    args.ovaConcepts.events.on('countRefresh', function(data){
        const star = document.querySelector('.learned-concepts .star')
        const classes = ['animated', 'glow']
        star.classList.remove(classes[0])
        setTimeout(function(){
            _.forEach(classes, function(className){
                star.classList.add(className)
            })
        }, 20)

        setTimeout(function(){
            star.classList.remove(classes[1])
        }, 750)
    })

    args.ovaProgress.events.on('update', function(data){
        _.forEach(args.ovaProgress.elements, function(element){
            element.classList.add('progressing')

            setTimeout(function(){
                element.classList.remove('progressing')
            }, 850)
        })
    })
}