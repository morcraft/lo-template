const _ = require('lodash');

module.exports = {
    animate: function(args){
        if(!_.isObject(args)){
            throw('Some arguments are expected.');
        }
        
        if(typeof args.$elements === 'undefined'){
            throw('Some elements are expected to animate.');
        }

        if(!args.$elements instanceof jQuery){
            throw('An instance of jQuery is expected.');
        }

        if(!args.$elements.length){
            console.warn('No elements were found to animate.');
            return true;
        }

        
        return args.$elements.each(function(){
            var delay = (Math.ceil(Math.random() * 120) * -1) + 's';
            $(this).addClass('cloud-animation')
            .css({
                '-webkit-animation-delay': delay,
                'animation-delay': delay
            });
        });

    }
}